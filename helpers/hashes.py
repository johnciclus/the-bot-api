import os, binascii


def generate_hex_hash():
    _id = str(binascii.b2a_hex(os.urandom(16)).decode('ascii'))
    return "-".join([_id[0:8], _id[8:12], _id[12:16], _id[16:20], _id[20:32]])


def generate_hex_user_id(addr):
    _id = str(binascii.hexlify("".join([addr, "user", "addr"]).encode('utf-8')).decode('ascii'))
    return "-".join([_id[0:8], _id[8:12], _id[12:16], _id[16:20], _id[20:32]])
