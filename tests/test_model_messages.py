from models.messages import find_one, find_all, find_by_conversation_id, create_one, delete_one


def test_messages_find_all():
    bots = find_all()
    assert type(bots) == list


def test_create_message():
    message = {
     "id": "16edd3b3-3f75-40df-af07-2a3813a79ce9",
     "conversationId": "7665ada8-3448-4acd-a1b7-d688e68fe9a1",
     "timestamp": "2018-11-16T23:30:52.6917722Z",
     "from": "36b9f842-ee97-11e8-9443-0242ac120002",
     "to": "16edd3b3-3f75-40df-af07-2a3813a79ce9",
     "text": "Oi! Como posso te ajudar?"
    }

    message["_id"] = message.get("id")
    result = create_one(message)
    del message["_id"]

    assert result.acknowledged
    assert result.inserted_id == "16edd3b3-3f75-40df-af07-2a3813a79ce9"


def test_find_one():
    message = find_one("16edd3b3-3f75-40df-af07-2a3813a79ce9")
    assert message.get("id") == "16edd3b3-3f75-40df-af07-2a3813a79ce9"
    assert message.get("text") == "Oi! Como posso te ajudar?"


def test_find_by_conversation_id():
    conversation_messages = find_by_conversation_id("7665ada8-3448-4acd-a1b7-d688e68fe9a1")

    assert type(conversation_messages) == list
    assert len(conversation_messages) > 0


def test_delete_message():
    message_id = "16edd3b3-3f75-40df-af07-2a3813a79ce9"
    result = delete_one(message_id)
    assert result.acknowledged
    assert result.deleted_count == 1
