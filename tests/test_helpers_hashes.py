from helpers.hashes import generate_hex_hash, generate_hex_user_id


def test_generate_hex_hash():
    hash = generate_hex_hash()
    assert len(hash) == 36
    assert hash[8] == '-'
    assert hash[13] == '-'
    assert hash[18] == '-'
    assert hash[23] == '-'


def test_generate_hex_user_idb():
    hash = generate_hex_user_id("127.0.0.1")
    assert len(hash) == 36
    assert hash[8] == '-'
    assert hash[13] == '-'
    assert hash[18] == '-'
    assert hash[23] == '-'
