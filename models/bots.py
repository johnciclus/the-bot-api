from configs.mongo import db


def find_one(bot_id):
    try:
        result = db["bots"].find_one({"id": bot_id}, {
            "_id": 0,
            "id": 1,
            "name": 1
        })
        return result
    except Exception as e:
        raise ValueError("Database Error: {}".format(e))


def find_all():
    try:
        result = db["bots"].find({}, {
            "_id": 0,
            "id": 1,
            "name": 1
        })
        return list(result)
    except Exception as e:
        raise ValueError("Database Error: {}".format(e))


def find_random():
    try:
        result = db["bots"].aggregate([
            {
                "$sample": {"size": 1}
            },
            {
                "$project": {
                    "_id": 0,
                    "id": 1,
                    "name": 1
                }
            }
        ])
        return list(result)[0]
    except Exception as e:
        raise ValueError("Database Error: {}".format(e))


def create_one(bot):
    try:
        return db["bots"].insert_one(bot)
    except Exception as e:
        raise ValueError("Database Error: {}".format(e))


def update_one(_id, bot):
    try:
        return db["bots"].update_one({"_id": _id}, {"$set": bot})
    except Exception as e:
        raise ValueError("Database Error: {}".format(e))


def delete_one(_id):
    try:
        return db["bots"].delete_one({"_id": _id})
    except Exception as e:
        raise ValueError("Database Error: {}".format(e))
