from configs.mongo import connect, db
from pymongo.database import Database


def test_mongo_connection():
    connection = connect()
    assert connection is not False
    assert type(connection) is Database


def test_mongo_db():
    assert type(db) is Database

