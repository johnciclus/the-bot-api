from flask import Flask
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


def create_app():
    """Create a Flask API."""

    app = Flask(__name__)

    app.config.from_object('config')

    from .routes import api

    app.register_blueprint(api)

    return app
