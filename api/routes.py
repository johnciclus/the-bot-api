import json
from flask import Blueprint, jsonify, request
from datetime import datetime
from models import bots, messages
from helpers.hashes import generate_hex_hash, generate_hex_user_id

api = Blueprint('api', __name__)

with open('knowledge_base.json') as data:
    knowledge_base = json.load(data)


@api.route('/')
def main():
    return 'The Bots API'


@api.route('/bots', methods=['GET'])
def get_all_bots():
    return jsonify(bots.find_all())


@api.route('/bots/<string:bot_id>', methods=['GET'])
def get_bot(bot_id: str):
    bot = bots.find_one(bot_id)
    if bot:
        return jsonify(bot)
    else:
        return "Bot not found", 404


@api.route('/bots', methods=['POST'])
def create_bot():
    bot = json.loads(request.data)
    bot["id"] = generate_hex_hash()

    if bot.get("name"):
        bot["_id"] = bot.get("id")

        try:
            response = bots.create_one(bot)
        except ValueError:
            return "Duplicate key, id: {}".format(bot.get("id")), 400

        if response.acknowledged:
            return "Bot with id {} was created".format(response.inserted_id)
        else:
            return "Bot not created", 400
    else:
        return "Bot's format is incomplete", 400


@api.route('/bots/<string:bot_id>', methods=['PUT'])
def update_bot(bot_id: str):
    bot = json.loads(request.data)

    if bot.get("name"):
        bot["id"] = bot_id

        try:
            response = bots.update_one(bot_id, bot)
        except ValueError:
            return "Error on key, id: {}".format(bot.get("id")), 400

        if response.acknowledged:
            return "Bot with id {} was updated".format(bot_id)
        else:
            return "Bot not updated", 400
    else:
        return "Bot's format is incomplete", 400


@api.route('/bots/<string:bot_id>', methods=['DELETE'])
def delete_bot(bot_id: str):
    try:
        response = bots.delete_one(bot_id)
    except ValueError:
        return "Error on key, id: {}".format(bot_id), 400

    if response.acknowledged:
        return "Bot with id {} was deleted".format(bot_id)
    else:
        return "Bot wasn't deleted", 400


@api.route('/messages', methods=['POST'])
def conversation():
    _id = generate_hex_hash()
    body = json.loads(request.data) if request.data else {}
    conversation_id = request.args.get('conversationId')
    conversation_messages = messages.find_by_conversation_id(conversation_id)
    timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%fZ')

    if not conversation_messages:
        conversation_id = generate_hex_hash()
        bot = bots.find_random()
        user_id = generate_hex_user_id(request.remote_addr)

        message = {
            "id": _id,
            "conversationId": conversation_id,
            "timestamp": timestamp,
            "from": bot.get("id"),
            "to": user_id,
            "text": knowledge_base.get("greetings")[0]
        }
    elif body.get("text"):
        last_message = conversation_messages[-1]

        message = {
            "id": _id,
            "conversationId": conversation_id,
            "timestamp": timestamp,
            "from": last_message.get("to"),
            "to": last_message.get("from"),
            "text": body.get("text")
        }
    else:
        last_message = conversation_messages[-1]
        bot_from = bots.find_one(last_message.get("from"))

        if bot_from:
            bot_id = last_message.get("from")
            user_id = last_message.get("to")
        else:
            bot_id = last_message.get("to")
            user_id = last_message.get("from")

        message = {
            "id": _id,
            "conversationId": conversation_id,
            "timestamp": timestamp,
            "from": bot_id,
            "to": user_id,
            "text": knowledge_base.get("undefined_questions")[0]
        }

    if message.get("id"):
        message["_id"] = message.get("id")
        messages.create_one(message)
        del message["_id"]

    return jsonify(message)


@api.route('/messages/<string:message_id>', methods=['GET'])
def get_message(message_id):
    message = messages.find_one(message_id)
    if message:
        return jsonify(message)
    else:
        return "Bot not found", 404


@api.route('/messages', methods=['GET'])
def get_conversation():
    conversation_id = request.args.get('conversationId')
    message = messages.find_by_conversation_id(conversation_id)
    if message:
        return jsonify(message)
    else:
        return "Bot not found", 404
