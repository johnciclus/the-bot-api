from models.bots import find_one, find_random, find_all, create_one, delete_one


def test_bots_find_all():
    bots = find_all()
    assert type(bots) == list


def test_bots_create_one():
    bot = {"_id": "3132372e-302e-302e-3175-736572616464", "id": "3132372e-302e-302e-3175-736572616464", "name": "BotTest"}
    result = create_one(bot)
    assert result.acknowledged
    assert result.inserted_id == "3132372e-302e-302e-3175-736572616464"


def test_bots_find_one():
    bot = find_one("3132372e-302e-302e-3175-736572616464")
    assert bot.get("id") == "3132372e-302e-302e-3175-736572616464"
    assert bot.get("name") == "BotTest"


def test_bots_find_random():
    bot = find_random()
    assert len(bot.get("id")) == 36
    assert type(bot.get("name")) == str


def test_bots_delete():
    bot_id = "3132372e-302e-302e-3175-736572616464"
    result = delete_one(bot_id)
    assert result.acknowledged
    assert result.deleted_count == 1



