# The Bot API

A platform that generates various types of bots automatically

**Autor**: John Freddy Garavito Suarez

**Technological stack**:

Language: Python 3
Version: 3.6.4
Database: Mongo 3.6 - Database as a Service – MongoLabs
Restful Client: Postman

**Installation**

1. Install Python 3 
2. Install Virtual environment
3. Install packages from requirements.txt
4. Open this project's root directory
5. Create and adding environment variables to .env
6. Run python server.py


**Description**

The Bot API is a HTTP/REST API where you can manage and consume bots, the two main routes are bots and messages which they allow the following functionalities:

**BOTS route**

**GET**: _/bots_

This route returns all bots in the database

**GET**: _/bots/bot_id_

This route returns the object that represents a specific bot in the database

**POST**: _/bots_

This route creates a new bot on the system, the request should have a body with the name attribute

**PUT**: _/bots/bot_id_

This route updates an existing bot in the database, it requires a body with name attribute

**DELETE**: _/bots/bot_id_

This route deletes an existing bot in the database referenced by the identifier bot_id  

**MESSAGES route**

**POST**: _/messages_

This route starts a new conversation with some bot selected automatically, the response will contain: 

* The conversation identifier (conversationId) that would allow to refer the conversation by means of a hash
* The identifiers of the entities in the conversation (from, to)
* The text replied by bot

**GET**: _/messages/message_id_

This route returns a message that is referenced by the message_id identifier 

**POST**: _/messages?conversationId=conversation_id_

This route continues the conversation referenced by conversation_id and requires that the body contains the text attribute

**GET**: _/messages?conversationId=conversation_id_

This route obtains the messages of the conversation in temporal order, allowing you to know the flow of the conversation

