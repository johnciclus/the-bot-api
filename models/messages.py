from configs.mongo import db


def find_one(message_id):
    try:
        result = db["messages"].find_one({"id": message_id}, {
            "_id": 0,
            "id": 1,
            "conversationId": 1,
            "timestamp": 1,
            "from": 1,
            "to": 1,
            "text": 1
        })
        return result
    except Exception as e:
        raise ValueError("Database Error: {}".format(e))


def find_by_conversation_id(conversation_id):
    try:
        result = db["messages"].find({"conversationId": conversation_id}, {
            "_id": 0,
            "id": 1,
            "conversationId": 1,
            "timestamp": 1,
            "from": 1,
            "to": 1,
            "text": 1
        }).sort("timestamp", 1)
        return list(result)
    except Exception as e:
        raise ValueError("Database Error: {}".format(e))


def find_all():
    try:
        result = db["messages"].find({}, {
            "_id": 0,
            "id": 1,
            "name": 1
        })
        return list(result)
    except Exception as e:
        raise ValueError("Database Error: {}".format(e))


def create_one(message):
    try:
        return db["messages"].insert_one(message)
    except Exception as e:
        raise ValueError("Database Error: {}".format(e))


def delete_one(_id):
    try:
        return db["messages"].delete_one({"_id": _id})
    except Exception as e:
        raise ValueError("Database Error: {}".format(e))
