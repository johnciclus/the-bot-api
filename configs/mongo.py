"""
    Description: Mongo Config File
"""

import os
import ssl
import pymongo
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

mongo_user = os.environ.get("MONGO_USER")
mongo_password = os.environ.get("MONGO_PASSWORD")
mongo_endpoint = os.environ.get("MONGO_ENDPOINT")
mongo_db = os.environ.get("MONGO_DB")


def connect():
    print("Connecting to MongoDB")

    if not mongo_user and not mongo_password and not mongo_endpoint and not mongo_db:
        raise Exception('Invalid MongoDB connection values')

    try:
        client = pymongo.MongoClient("mongodb://{}:{}@{}/{}".format(
            mongo_user,
            mongo_password,
            mongo_endpoint,
            mongo_db),
            ssl_cert_reqs=ssl.CERT_NONE)
        print("MongoDB connected")

    except pymongo.errors.ConnectionFailure as err:
        print(err)

    return client[mongo_db] if client else False


db = connect()

